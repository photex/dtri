;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; dtri.lisp
;;
;; I'm trying to learn and understand how to tessellate a set of random
;; points and so have created this program as an exercise. It may or may
;; not be "correct" at any given time and I'm no authority on the subject
;; so please don't rely on this for anything other than curiosity.
;; 
;; This program uses opengl and glut to perform a basic (dumb) visualizatio
;; of the set and the resulting triangulation.
;;
;; Middle mouse drag: Pan
;; Right mouse drag: Zoom
;;
;; This program and it's source are made available to the public
;; under the MIT license.
;; 
;; - photex (Chip Collier)
;;

(require :sb-cga)
(require :cl-graph)
(require :cl-opengl)
(require :cl-glut)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; structs

(defstruct edge
  (v0 #() :type sb-cga:vec)
  (v1 #() :type sb-cga:vec))

(defstruct circle
  (center nil :type sb-cga:vec)
  (radius 0.0 :type float)
  (radius-squared 0.0 :type float)
  (diameter 0.0 :type float))

(defstruct triangle
  (verts #() :type vector)
  (circumcircle nil :type circle))

(defstruct box
  (min nil :type sb-cga:vec)
  (max nil :type sb-cga:vec))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun vec-distance-3d (a b &key squared)
  (let* ((diff (sb-cga:vec- a b))
         (result (+ (expt (aref diff 0) 2)
                    (expt (aref diff 1) 2)
                    (expt (aref diff 2) 2))))
    (if squared
        result
        (sqrt result))))

(defun vec-distance-2d (a b &key squared)
  (let* ((diff (sb-cga:vec- a b))
         (result (+ (expt (aref diff 0) 2)
                    (expt (aref diff 1) 2))))
    (if squared
        result
        (sqrt result))))

(defun vec-midpoint (a b)
  (let* ((c (sb-cga:vec+ a b)))
    (sb-cga:vec/ c 2.0)))

(defun vec-in-diametral-circle? (center diameter p)
  (let* ((square-distance (vec-distance-2d center p :squared t)))
    (< square-distance diameter)))

(defun vec-in-diametral-sphere? (center diameter p)
  (let* ((square-distance (vec-distance-3d center p :squared t)))
    (< square-distance diameter)))

(defun calc-circumcircle (v0 v1 v2)
  "Returns a circle struct representing the circumcircle of the given 3 verts"
  (let* ((diff-v1-v0 (sb-cga:vec- v1 v0))
         (diff-v2-v0 (sb-cga:vec- v2 v0))
         (diff-v2-v1 (sb-cga:vec- v2 v1))
         (add-v1-v0 (sb-cga:vec+ v1 v0))
         (add-v2-v0 (sb-cga:vec+ v2 v0))
         (a (aref diff-v1-v0 0))
         (b (aref diff-v1-v0 1))
         (c (aref diff-v2-v0 0))
         (d (aref diff-v2-v0 1))
         (e (+ (* a (aref add-v1-v0 0))
               (* b (aref add-v1-v0 1))))
         (f (+ (* c (aref add-v2-v0 0))
               (* d (aref add-v2-v0 1))))
         (g (* 2.0 (- (* a (aref diff-v2-v1 1))
                      (* b (aref diff-v2-v1 0)))))
         (collinear? (< (abs g) sb-cga:+default-epsilon+))
         (cx 0.0) (cy 0.0) (dx 0.0) (dy 0.0))
    (if collinear?
        (progn
          (let ((minx (min (aref v0 0) (aref v1 0) (aref v2 0)))
                (miny (min (aref v0 1) (aref v1 1) (aref v2 1)))
                (maxx (max (aref v0 0) (aref v1 0) (aref v2 0)))
                (maxy (max (aref v0 1) (aref v1 1) (aref v2 1))))
            (setf cx (/ (+ minx maxx) 2))
            (setf cy (/ (+ miny maxy) 2))
            (setf dx (- cx minx))
            (setf dy (- cy miny))))
        (progn
          (setf cx (/ (- (* d e) (* b f)) g))
          (setf cy (/ (- (* a f) (* c e)) g))
          (setf dx (- cx (aref v0 0)))
          (setf dy (- cy (aref v0 1)))))
    (let* ((radius-squared (+ (* dx dx)
                              (* dy dy)))
           (radius (sqrt radius-squared)))
      (make-circle :center (sb-cga:vec cx cy 0.0)
                   :radius radius
                   :radius-squared radius-squared
                   :diameter (* radius 2)))))

(defun new-triangle (vi0 vi1 vi2 points)
  (let ((v0 (aref points vi0))
        (v1 (aref points vi1))
        (v2 (aref points vi2)))
    (make-triangle :verts (make-array 3 :initial-contents (list vi0 vi1 vi2))
                   :circumcircle (calc-circumcircle v0 v1 v2))))

(defun get-min-max (point-set)
  "Return the min and max vectors for the given point set."
  ;; This is kinda ugly and I wrote this without even considering an
  ;; implementation using reduce. From what I can tell it's essentially
  ;; faster than the other implementation and conses fewer bytes.
  (let* ((first-point (aref point-set 0))
         (rest-points (subseq point-set 1))
         (minx (aref first-point 0)) (maxx (aref first-point 0))
         (miny (aref first-point 1)) (maxy (aref first-point 1))
         (minz (aref first-point 2)) (maxz (aref first-point 2)))
    (loop :for p :across rest-points :do
         (setf minx (min minx (aref p 0))) (setf maxx (max maxx (aref p 0)))
         (setf miny (min miny (aref p 1))) (setf maxy (max maxy (aref p 1)))
         (setf minz (min minz (aref p 2))) (setf maxz (max maxz (aref p 2))))
    (values (sb-cga:vec minx miny minz) (sb-cga:vec maxx maxy maxz))))

(defun get-bounding-box (point-set)
  "Returns a box whose area/volume contains the point-set."
  (multiple-value-bind (min max) (get-min-max point-set)
    (make-box :min min :max max)))

(defun get-bounding-triangle-coords (point-set &optional (fudge-factor 10))
  (multiple-value-bind (min max) (get-min-max point-set)
    (let ((dx (* fudge-factor (- (aref max 0) (aref min 0))))
          (dy (* fudge-factor (- (aref max 1) (aref min 1)))))
      (make-array 3 :initial-contents
                  (list (sb-cga:vec (- (aref min 0) dx) (- (aref min 1) (* dy 3)) 0.0)
                        (sb-cga:vec (- (aref min 0) dx) (+ (aref max 1) dy) 0.0)
                        (sb-cga:vec (+ (aref max 0) (* dx 3)) (+ (aref max 1) dy) 0.0))))))

(defun in-circumcircle? (tri p)
  (let* ((circumcircle (slot-value tri 'circumcircle))
         (center (slot-value circumcircle 'center))
         (dist-squared (vec-distance-2d center p :squared t)))
    (<= dist-squared (slot-value circumcircle 'radius-squared))))

(defun edge= (a b)
  (or (and (= (first a) (first b))
           (= (second a) (second b)))
      (and (= (first a) (second b))
           (= (second a) (first b)))))

(defun unique-edge? (a edges)
  (let ((instance-count (length (remove-if-not (lambda (b) (edge= a b)) edges))))
    (<= instance-count 1)))

(defun shares-points? (a b)
  (let* ((verts (slot-value a 'verts))
         (bverts (slot-value b 'verts))
         (v0 (aref verts 0))
         (v1 (aref verts 1))
         (v2 (aref verts 2))
         (bv0 (aref bverts 0))
         (bv1 (aref bverts 1))
         (bv2 (aref bverts 2)))
    (or (= bv0 v0) (= bv0 v1) (= bv0 v2)
        (= bv1 v0) (= bv1 v1) (= bv1 v2)
        (= bv2 v0) (= bv2 v1) (= bv2 v2))))

(defun add-vertex (vi triangles points)
  (let* ((edges ())
         (unaffected-tris ()))
    ;; For each triangle in the list we take the edges
    ;; of any triangle where vert is inside it's circumcircle
    ;; and append it to the edges list. Otherwise the triangle
    ;; is collected and stored in unaffected-tris
    (setf unaffected-tris
          (loop for tri in triangles
             if (in-circumcircle? tri (aref points vi))
             do (let* ((verts (slot-value tri 'verts))
                       (e0 (list (aref verts 0) (aref verts 1)))
                       (e1 (list (aref verts 1) (aref verts 2)))
                       (e2 (list (aref verts 2) (aref verts 0))))
                  (setf edges (append edges (list e0 e1 e2))))
             else collect tri))
    
    ;; Remove any edges that are duplicate so that the edge
    ;; list only contains the boundary edges.
    (setf edges (loop for edge in edges
                   if (unique-edge? edge edges) collect edge))
    
    ;; Using the remaining edges and our input vert create
    ;; new triangles and return them appended to our unaffected-tris list
    (append unaffected-tris (loop for edge in edges
                               collect (let ((vi0 (first edge))
                                             (vi1 (second edge)))
                                         (new-triangle vi0 vi1 vi points))))))

(defun triangulate (point-set)
  (let* (;; Add the coords for a large bounding triangle to the point set
         (st-coords (get-bounding-triangle-coords point-set))
         (sti0 (length point-set))
         (sti1 (1+ sti0))
         (sti2 (1+ sti1))
         (points (concatenate 'vector point-set st-coords))
         
         ;; Create the bounding triangle instance
         (supertri (new-triangle sti0 sti1 sti2 points))
         ;; Initialize our triangle list
         (triangles (list supertri)))
    
    ;; For each point in the list we get an updated set
    ;; of triangles by retesselating using the new point
    (loop for i below (length points)
       do (setf triangles (add-vertex i triangles points)))
    
    ;; Remove any triangles that share points with the super triangle
    (remove-if (lambda (a) (shares-points? a supertri)) triangles)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Utils

(defun make-random-point (&optional (state *random-state*))
  "Return an instance of sb-cga:vec initialized with random values."
  (apply #'sb-cga:vec (loop repeat 3
                         collect (random 1.0 state))))

(defun make-random-point% (&optional (state *random-state*))
  "Return an instance of sb-cga:vec initialized with random values."
  (sb-cga:vec (random 1.0 state)
              (random 1.0 state)
              (random 1.0 state)))

(defun make-random-point-set (count &optional (state *random-state*))
  "Returns an array of <count> random-points."
  (let ((result (make-array count :fill-pointer 0)))
    (dotimes (i count)
      (vector-push (make-random-point state) result))
    result))

(defmacro sort-set-by (point-set index)
  "Sort the input point set by the value in the specified index."
  `(sort ,point-set #'< :key (lambda (p) (aref p ,index))))

(defmacro sort-set-by-x (point-set)
  "Sort the input point set by the value at 0"
  `(sort-set-by ,point-set 0))

(defmacro sort-set-by-y (point-set)
  "Sort the input point set by the value at 1"
  `(sort-set-by ,point-set 1))

(defmacro sort-set-by-z (point-set)
  "Sort the input point set by the value at 2"
  `(sort-set-by ,point-set 2))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Windowing

(defclass viz-window (glut:window)
  ((point-set :initform ())
   (triangles :initform ())
   (zoom :initform 0.0025)
   (offset :initform #(0.0 0.0))
   (last-mouse :initform ())
   (pan-throttle :initform 5)
   (zoom-throttle :initform 0.00025)
   (update-mode :initform nil))
  (:default-initargs :pos-x 100 :pos-y 100
                     :width 800 :height 800
                     :mode '(:double :rgba)
                     :title "dtri-viz"))

(defmethod glut:display-window :before ((w viz-window))
  (gl:clear-color 0.15 0.15 0.15 0)
  (gl:enable :blend)
  (gl:blend-func :one :one-minus-src-alpha)
  (gl:polygon-mode :front-and-back :line))

(defmethod glut:mouse ((w viz-window) button state x y)
  (with-slots (update-mode last-mouse) w
    (setf update-mode
          (when (eq state :down)
            (setf last-mouse (list x y))
            (case button
              (:right-button :zoom)
              (:middle-button :pan))))))

(defmethod glut:motion ((w viz-window) x y)
  (with-slots (offset zoom last-mouse update-mode zoom-throttle pan-throttle) w
    (let* ((dx (- (car last-mouse) x))
           (dy (- (cadr last-mouse) y))
           (modx (* (/ dx (glut:width w)) 100))
           (mody (* (/ dy (glut:height w)) 100))
           (effective-pan-speed (* pan-throttle zoom)))
      (case update-mode
        (:zoom
         (setf zoom (max 0.0001 (+ zoom (* zoom-throttle modx)))))
        (:pan
         (setf (aref offset 0) (+ (aref offset 0) (* effective-pan-speed modx))
               (aref offset 1) (- (aref offset 1) (* effective-pan-speed mody))))))
    (setf last-mouse (list x y)))
  (glut:post-redisplay))

(defmethod glut:keyboard ((w viz-window) key x y)
  (declare (ignore x y))
  (case key
    (#\Esc (glut:destroy-current-window))))

(defmethod glut:reshape ((w viz-window) width height)
  (setf (glut:width w) width
        (glut:height w) height)
  (gl:viewport 0 0 width height)
  (glut:post-redisplay))

(defun draw-circle (center radius)
  (let* ((step 5)
         (seg-max (- 360 step))
         (trig-step 0.0174))
    (gl:with-primitive :line-loop
      (loop for i from 0 upto seg-max by step
         do (let ((x (+ (aref center 0) (* radius (sin (* trig-step i)))))
                  (y (+ (aref center 1) (* radius (cos (* trig-step i))))))
              (gl:vertex x y 0))))))

(defmethod glut:display ((w viz-window))
  (gl:clear :color-buffer)

  (with-slots (point-set triangles offset zoom) w

    ;; Calculate the view
    (gl:matrix-mode :projection)
    (let* ((width (glut:width w))
           (height (glut:height w))
           (hx (* (* width zoom) 0.5))
           (hy (* (* height zoom) 0.5))
           (xoffset (aref offset 0))
           (yoffset (aref offset 1)))
      (gl:load-identity)
      (gl:ortho (- xoffset hx) (+ xoffset hx)
                (- yoffset hy) (+ yoffset hy)
                -5 5))

    ;; I'm assuming that the point set was created with the
    ;; functions above which are random from 0-1. Offsetting
    ;; the modelview matrix to center them in the viewport
    (gl:matrix-mode :modelview)
    (gl:load-identity)
    (gl:translate -0.5 -0.5 0)

    ;; ;; Draw the unit grid/ground/blah/foo/bar
    (gl:color 1 0.25 0.25 0.25)
    (gl:line-width 2)
    (gl:rect 0 0 1 1)

    ;;; Draw the triangulation
    (loop for tri in triangles
       do (progn
            ;; Draw the circumcircles
            (gl:color 0.15 0.05 0.15 0.05)
            (gl:line-width 1)
            (let ((cir (slot-value tri 'circumcircle)))
              (draw-circle (slot-value cir 'center) (slot-value cir 'radius)))

            ;; Draw the graph
            (gl:color 0 0.25 1 0.25)
            (gl:line-width 2)
            (gl:with-primitive :triangles
              (let ((verts (slot-value tri 'verts)))
                (loop for i from 0 upto 2
                   do (let* ((vi (aref verts i))
                             (v (aref point-set vi)))
                        ;(print v)
                        (gl:vertex (aref v 0) (aref v 1) (aref v 2))
                        ))))))

    ;;; Draw the points
    (gl:color 1 1 1 1)
    (gl:point-size 5)  
    (gl:with-primitive :points
      (loop for i from 0 upto (- (length point-set) 1)
         do (let* ((p (aref point-set i))
                   (x (aref p 0))
                   (y (aref p 1)))
              (gl:vertex x y 1))))
    
    (gl:flush))
  
  
  (glut:swap-buffers))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; "Top level" functions.

(defun dtri-viz (point-set triangles)
  (let ((w (make-instance 'viz-window)))
    (setf (slot-value w 'point-set) point-set)
    (setf (slot-value w 'triangles) triangles)
    (glut:display-window w)))

(defun random-graph (num-points)
  (let ((points (make-random-point-set num-points)))
    (dtri-viz points (triangulate (sort-set-by-x points)))))

![ScreenShot](http://i.imgur.com/OH7Cq48.png)

I set out to learn how to implement delaunay triangulation in Common Lisp and this repository is the result.

This code is covered by the MIT license so please feel free to take it and run if you can make use of it.


To quickly load and see a window with a random triangulation, run your lisp and:

`(ql:quickload '(:cl-opengl :cl-glut :sb-cga :cl-graph))`

`(load "dtri.lisp")`

`* (random-graph 30)`

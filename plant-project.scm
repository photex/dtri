((#:quickloads
  "swank"
  "alexandria"
  "cl-opengl"
  "cl-glut"
  "sb-cga"
  "cl-graph")
 (#:plant-lisp . "ccl64")
 (#:project-name . "dtri")
 (#:no-user-init . "-n")
 (#:load . "-l")
 (#:eval . "-e")
 (#:save
  .
  "'(save-application #P\".plant/ccl64-dtri\" :prepend-kernel t :purify t)'"))
